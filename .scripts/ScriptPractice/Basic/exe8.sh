#!bin/bash

FILES=$@

for item in $FILES
do
    if [ -d $item ]
    then
        echo "$item is a directory"
    elif [ -f $item ]
    then
        echo "$item is a regular file"
    else
        echo "$item is other type of file"
    fi
done
