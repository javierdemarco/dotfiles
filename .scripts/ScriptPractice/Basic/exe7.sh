#!bin/bash

FILE=$1

if [ -d $FILE ]
then
    echo "$FILE is a directory"
elif [ -f $FILE ]
then
    echo "$FILE is a regular file"
else
    echo "$FILE is other type of file"
fi
