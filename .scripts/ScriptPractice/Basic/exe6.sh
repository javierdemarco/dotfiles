#!bin/bash

read -p "Enter a filename or a directory name: " filename

if [ -d $filename ]
then
    echo "$filename is a directory"
elif [ -f $filename ]
then
    echo "$filename is a regular file"
else
    echo "$filename is other type of file"
fi
