#!/bin/bash

file_count (){
    local DIR=$1
    if [ -d $DIR ]
    then
        echo "$DIR:"
        echo $(ls -1 $DIR | wc -l)
    else 
        return 1
    fi
}

file_count $1

