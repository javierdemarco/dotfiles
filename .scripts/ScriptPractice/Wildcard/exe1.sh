#!/bin/bash
DATE=$(date +"%Y-%m-%d")
FILES=$(ls *.jpg)

for IMG in $FILES
do
    mv "$IMG" "$DATE-$IMG"
done

