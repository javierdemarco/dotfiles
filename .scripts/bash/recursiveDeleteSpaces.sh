#!/usr/bin/env bash

PATH="$HOME/pCloudDrive/Books"

/bin/find $PATH -name "*.epub" -print0 | /bin/xargs -0 -I {} rename " " "_" {}
