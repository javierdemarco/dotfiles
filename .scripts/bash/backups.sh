#!/bin/bash

sudo rsync -aAXv --delete --exclude='/dev/*' --exclude='/proc/*' --exclude='/sys/*' --exclude='/tmp/*' --exclude='/run/*' --exclude='/mnt/*' --exclude='/media/*' --exclude='swapfile' --exclude='lost+found' --exclude='.cache' --exclude='Downloads' --exclude='pCloudDrive' --exclude='.ecryptfs' --exclude='Desktop' --exclude='Pictures' --exclude='.pcloud' --exclude='timeshift' / /home/javier/pCloudDrive/Backups/Arch
