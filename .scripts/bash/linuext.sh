#!/bin/bash

# LINUX EXTRACTION SCRIPT
# Description: this script extract all the important forensic information stored in a Linux System
#              for forensic analysis.
#              It will create a subsystem containing the exact file structure from the original
#              machine. 
#              It will also create a /result folder for the output of several commands
#              which are usefull.
# Author: Javier de Marco Tomas
# Version: 0.1
# Date: 18/May/2021

# Directory variables
OUT_DIR="linuext_output"
SYSTEM_INFO_DIR="${OUT_DIR}/System_info"
NETWORK_DIR="${OUT_DIR}/Network"
LOGS_DIR="${OUT_DIR}/Logs"
USERS_DIR="${OUT_DIR}/Users"
STARTUP_DIR="${OUT_DIR}/Startup"
SCHEDULE_DIR="${OUT_DIR}/ScheduleTasks"
PROCESSES_DIR="${OUT_DIR}/Processes"
FULLFILELISTING_DIR="${OUT_DIR}/FullFileListing"

# Files arrays
ETC_SYSTEM_FILES=(hostname timezone profile bash.bashrc)
PROC_FILES=(version)
USER_FILES=(.bash_history .cache .bashrc .config .mozilla .local .profile .bash_login .bash_profile)

#  Axiliar variables
USERS_HOME=$(grep "/home" /etc/passwd | cut -d ":" -f1)
COMPRESSED_FILE="linux_extraction_result.zip"

echo "---- INICIATING LINUX EXTRACTION ----"

if [ "$1" == "-h" ]
then
    echo "Manual for LINUX EXTRACTION SCRIPT:"
    echo "This script will extract all the important forensic information stored in a Linux System
    for forensic analysis."
    echo "The output will be created on the directory from which this script is run with name \"${OUT_DIR}\""
    echo "Example: \"sudo $0\""
    echo "Manual: \"$0 -h\""
    exit
fi

echo "---- Checking for sudo privileges ----"
if [ "$EUID" -ne 0 ]
  then echo "---- Please run as root ----"
  exit
fi

# Creating directories
mkdir $OUT_DIR
mkdir $SYSTEM_INFO_DIR
mkdir $NETWORK_DIR
mkdir $LOGS_DIR
mkdir $USERS_DIR
mkdir "${USERS_DIR}"/root
mkdir "${SCHEDULE_DIR}"
mkdir -p "${STARTUP_DIR}/Applications"
mkdir "${PROCESSES_DIR}"
mkdir "${FULLFILELISTING_DIR}"

echo "---- Getting the information from /etc folder ----"
cat /etc/*-release > "${SYSTEM_INFO_DIR}"/system_info.txt
for FILE in "${ETC_SYSTEM_FILES[@]}"
do
    dd if=/etc/"${FILE}" of="${SYSTEM_INFO_DIR}"/"${FILE}" status=none
done
rsync -a /etc/init.d /etc/rc*.d "${STARTUP_DIR}/Applications/" 2>/dev/null
systemd-analyze blame > "${STARTUP_DIR}/boot_analyze.txt"
rsync -a /etc/cron* "${SCHEDULE_DIR}" 2>/dev/null
echo "---- Finished with /etc ----"

echo "---- Getting /var folder ----"
rsync -a /var/log/ "${LOGS_DIR}" 2>/dev/null
rsync -a /var/spool/cron* "${SCHEDULE_DIR}" 2>/dev/null
echo "---- Finished with /var ----"

echo "---- Getting the information from /proc ----"
for FILE in "${PROC_FILES[@]}"
do
    dd if=/proc/"${FILE}" of="${SYSTEM_INFO_DIR}"/"${FILE}".txt status=none
done
uptime > "${SYSTEM_INFO_DIR}"/uptime.txt
echo "---- Finished with /proc ----"

echo "---- Getting the information of installation date ----"
echo "Please provide the name of the main partition (where the OS is installed)"
echo "This is the output of the partions: (Usually is the one with Type Linux)"
echo ""
fdisk -l
read -r MAIN
tune2fs -l /dev/"$MAIN" | grep "Filesystem created:" > "${SYSTEM_INFO_DIR}"/installation_date.txt
echo "---- Finished with installation date ----"

echo "---- Getting information about network ----"
ip a > "${NETWORK_DIR}"/network_info.txt
netstat -anp > "${NETWORK_DIR}"/socket_info.txt
netstat -rn > "${NETWORK_DIR}"/routing_table.txt
echo "---- Finished with information about network ----"

echo "---- Information about Regular Users ----"
grep "/home" /etc/passwd > "${USERS_DIR}/regular_users.txt"
for USER in "${USERS_HOME[@]}"
do
    mkdir "${USERS_DIR}/${USER}"
    for FILE in "${USER_FILES[@]}"
    do
        rsync -a /home/"${USER}"/"${FILE}" "${USERS_DIR}"/"${USER}"/"${FILE}" 2>/dev/null
    done
done
last > "${USERS_DIR}/loggin_history.txt"
lastb >"${USERS_DIR}/loggin_history_with_failure.txt"
w > "${USERS_DIR}/users_logged.txt"
echo "---- Finished with Regular Users ----"

echo "---- Information about Root ----"
grep ":0" /etc/passwd  > "${USERS_DIR}/root_users.txt"
for FILE in "${USER_FILES[@]}"
do
    rsync -a /root/"${FILE}" "${USERS_DIR}"/root/"${FILE}" 2>/dev/null
done
echo "---- Finished with Root User ----"

echo "---- Information about all Groups and all Users ----"
cat /etc/group  > "${USERS_DIR}/groups.txt"
grep -v "nologin" /etc/passwd > "${USERS_DIR}/list_users.txt"
cat /etc/sudoers > "${USERS_DIR}/sudoers.txt"
echo "---- Finished with all Groups all Users ----"

echo "---- Creating fullfile listing with dates ----"
find / -type f -print0 2>/dev/null | xargs -0 ls -l --time-style="+%F %T" > "${FULLFILELISTING_DIR}/MFTLike_linux.txt" 2>/dev/null
ls -Rla / 2>/dev/null > "${FULLFILELISTING_DIR}/FullFileListing.txt" 2>/dev/null
echo "---- Finished Creation of fullfile listing with dates ----"

echo "---- Processes in memory ----"
ps -ef > "${PROCESSES_DIR}/processes_memory.txt"
echo "---- Finished with processes in memory ----"

echo "---- Getting general information ----"
df -h > "${SYSTEM_INFO_DIR}/mounted_hard_drives.txt"
lsof -V > "${FULLFILELISTING_DIR}/opened_files.txt" 2>/dev/null
echo "---- Finished with general information ----"

echo "---- Creating compressed file ----"
zip -qdgds 200m "${COMPRESSED_FILE}" -r "${OUT_DIR}"
echo "---- Finished compression of data ----"

echo "---- FINISHED EXTRACTION ----"
