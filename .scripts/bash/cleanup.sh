#!/bin/bash

echo "Cleaning Cache..."
paccache -r
rm -rf ~/.cache/*  

echo "Removing Orphans..."
pacman -Rns $(pacman -Qtdq)

