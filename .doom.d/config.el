;;; Doom Configuration
;;; Author: Javier de Marco

;; Some usefull Defaults
(setq-default delete-by-moving-to-trash t                      ; Delete files to trash
              window-combination-resize t                      ; take new window space from all other windows (not just current)
              x-stretch-cursor t                               ; Stretch cursor to the glyph width
              history-length 1000)

;; Variable set up
(setq user-full-name "Javier de Marco"
      user-mail-address "javierdemarcoo@gmail.com"
      doom-font (font-spec :family "CaskaydiaCove Nerd Font" :size 20)
      doom-big-font (font-spec :family "CaskaydiaCove Nerd Font" :size 32)
      doom-variable-pitch-font (font-spec :family "CaskaydiaCove Nerd Font" :size 20)
      doom-theme 'doom-vibrant
      undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      auto-save-default t                         ; Nobody likes to loose work, I certainly don't
      truncate-string-ellipsis "…"                ; Unicode ellispis are nicer than "...", and also save /precious/ space
      scroll-margin 2                             ; It's nice to maintain a little margin
      evil-vsplit-window-right t                  ; Default position for split windows
      evil-split-window-below t
      display-line-numbers-type 'relative
      which-key-idle-delay 0.5
      org-directory "~/Documents/org/")

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(global-subword-mode 1)                           ; Iterate through CamelCase words
(rainbow-mode)

(defadvice! prompt-for-buffer (&rest _)           ; Ask wich buffer insert in new window
  :after '(evil-window-split evil-window-vsplit)
  (consult-buffer))

(general-define-key
 "C-M-l" 'centaur-tabs-forward
 "C-M-k" 'centaur-tabs-forward
 "C-M-j" 'centaur-tabs-backward
 "C-M-h" 'centaur-tabs-backward
 "M-j"   "jzz"
 "M-k"   "kzz")

(defun doom-modeline-conditional-buffer-encoding ()    ; Remove Encoding in modeline
  "We expect the encoding to be LF UTF-8, so only show the modeline when this is not the case"
  (setq-local doom-modeline-buffer-encoding
              (unless (and (memq (plist-get (coding-system-plist buffer-file-coding-system) :category)
                                 '(coding-category-undecided coding-category-utf-8))
                           (not (memq (coding-system-eol-type buffer-file-coding-system) '(1 2))))
                t)))

(add-hook 'after-change-major-mode-hook #'doom-modeline-conditional-buffer-encoding)

;; (custom-set-faces!
  ;; '(doom-modeline-buffer-modified :foreground "orange"))

;(load-file "personal.el")
